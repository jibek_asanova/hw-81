import LinkShortener from "./containers/LinkShortener/LinkShortener";

const App = () => (
    <div>
      <LinkShortener/>
    </div>
);

export default App;
