import {
    CREATE_LINK_FAILURE,
    CREATE_LINK_REQUEST,
    CREATE_LINK_SUCCESS,
} from "./linkActions";


const initialState = {
    shortUrl : ''
};

const linkReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_LINK_REQUEST:
            return {...state, singleLoading: true};
        case CREATE_LINK_SUCCESS:
            return {...state,  singleLoading: false, shortUrl: action.payload.shortUrl};
        case CREATE_LINK_FAILURE:
            return {...state, singleLoading: false};
        default:
            return state;
    }
};

export default linkReducer;