import axios from "axios";

export const CREATE_LINK_REQUEST = 'CREATE_LINK_REQUEST';
export const CREATE_LINK_SUCCESS = 'CREATE_LINK_SUCCESS';
export const CREATE_LINK_FAILURE = 'CREATE_LINK_FAILURE';

export const createLinkRequest = () => ({type: CREATE_LINK_REQUEST});
export const createLinkSuccess = data => ({type: CREATE_LINK_SUCCESS, payload: data});
export const createLinkFailure = () => ({type: CREATE_LINK_FAILURE});

export const createLink = data => {
    return async dispatch => {
        try {
            dispatch(createLinkRequest());
            const dataLink = await axios.post('http://localhost:8000/links', data);
            dispatch(createLinkSuccess(dataLink.data));
        } catch (e) {
            dispatch(createLinkFailure());
            throw e;
        }
    };
};

