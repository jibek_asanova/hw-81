import React from 'react';
import SearchInput from "../../components/SearchInput/SearchInput";
import {useDispatch, useSelector} from "react-redux";
import {createLink} from "../../store/linkActions";
import {Grid, Link, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles({
    root: {
        marginTop: '20px',
        textAlign: 'center',
        margin: '0 auto',
        display: 'block'
    },
});

const LinkShortener = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const shortUrl = useSelector(state => state.shortUrl);

    const onSubmit = async data => {
        await dispatch(createLink(data));
    };

    return (
        <div>
            <SearchInput
                onSubmit={onSubmit}
            />
            <Grid item xs>
                <Typography variant="h5" className={classes.root}>Your link now look like this:</Typography>
            </Grid>
            {shortUrl && <Grid item xs>
                <Link href={'http://localhost:8000/links/' + shortUrl} className={classes.root} target="_blank">{'http://localhost:8000/links/' + shortUrl}</Link>
            </Grid>}
        </div>
    );
};

export default LinkShortener;