import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";


const useStyles = makeStyles({
    root: {
        marginTop: '20px',
        textAlign: 'center',
        margin: '0 auto',
        padding: '0 100px'
    },
});

const SearchInput = ({onSubmit}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        url: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        onSubmit({...state});
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        <div>
            <Grid item xs>
                <Typography variant="h4" className={classes.root}>Shorten your link!</Typography>
            </Grid>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                onSubmit={submitFormHandler}
            >
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Enter your URL here"
                        name="url"
                        value={state.url}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Shorten</Button>
                </Grid>
            </Grid>
        </div>
    );
};

export default SearchInput;