const express = require('express');
const Link = require('../models/Link');
const {nanoid} = require("nanoid");

const router = express.Router();

router.post('/', async (req, res) => {
  if (!req.body.url) {
    return res.status(400).send({error: 'Data not valid'});
  }
  const linkData = {
    url: req.body.url,
  };

  linkData.shortUrl = nanoid(7);

  const link = new Link(linkData);
  try {
    await link.save();
    res.send(link);
  } catch {
    res.sendStatus(400).send({error: 'Data not valid'});
  }
});

router.get('/:shortUrl', async (req, res) => {
  try {
    const link = await Link.findOne({shortUrl: req.params.shortUrl});
    if (link) {
      res.status(301).redirect(link.url)
    } else {
      res.sendStatus(404).send({error: 'Product not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

module.exports = router;