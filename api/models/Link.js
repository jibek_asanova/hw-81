const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
  url: {
    type: String,
    required: true,
  },
  shortUrl: String
});


const Link = mongoose.model('Link', LinkSchema);
module.exports = Link;